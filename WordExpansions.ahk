;---->Word extension used to improve typing
; This section contains word extensions and spelling corrections

;spelling mistakes
::nad::and

;lazy typing :)
::hyd-::hydraulic
::sys-::system
::rel-::reliability
::rmt-::Reliability, Maintainability & Testability
::stats-::statistics
::bayes-::Bayesian
::eng-::engineering
::dist-::distribution
::desc-::description

;----<