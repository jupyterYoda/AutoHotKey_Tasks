; IMPORTANT INFO ABOUT GETTING STARTED: Lines that start with a
; semicolon, such as this one, are comments.  They are not executed.

;---->Program Launcher
;This set of code launches programs with the right control key and the number pad

; open notepadd++
RCtrl & n::
 IfWinExist Untitled - Notepad++
	WinActivate
else
	Run Notepad++
return
